import os
import platform
import shutil

def validate_os():
    if platform.system() == "Windows":
        return 0
    else:
        exit('This software should only be used on Windows 10')


def clear_folders(whitelist):
    print(whitelist)

    blacklist = [f.path for f in os.scandir(os.dir()) if f.is_dir() and f not in whitelist] 

    for i in blacklist:
        shutil.rmtree(i)

    return blacklist

def del_reg_keys(keys):
    None

if __name__ == "__main__":
    validate_os()
    with open("whitelist.txt") as f:
        content = f.readlines()
    
    content = [x.strip() for x in content] 

    requires_clearing = clear_folders(content)
    
    del_reg_keys(requires_clearing)